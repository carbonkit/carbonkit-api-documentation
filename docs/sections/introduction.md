Introduction
============

About This Manual
-----------------

This manual is designed to show you how to use the CarbonKit API to embed
environmental intelligence from the CarbonKit platform into your applications or systems. This allows you to perform carbon or other
sustainability-related calculations without having to find all the
numbers and algorithms yourself.

This manual is made available under a [Creative Commons
Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/), which means you can change and modify it if you want to. The [source code](https://gitlab.com/carbonkit/carbonkit-api-documentation) is available from GitLab. Improvements are welcomed!

Before You Start
----------------

As the CarbonKit API works over HTTP, we assume that you are familiar with the operation of HTTP requests, as well as common formats like XML and JSON.
In some cases we use XPath and JSONPath as well.

API Versions
------------

This manual only covers version 3.6 of the the CarbonKit API. For details of API versioning, see [Specifying API
Versions](advanced.md#specifying-api-versions).
