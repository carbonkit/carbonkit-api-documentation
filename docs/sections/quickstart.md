Quick Start
===========

If you want to calculate carbon emissions (or other sustainability
metrics) in your applications, the CarbonKit API is for you. This quick start guide will
give you the bare minimum you need to get started.

Getting Started
---------------

Before you start, you'll need an account, which you can get by
signing up at [www.carbonkit.net](https://www.carbonkit.net).

The CarbonKit API is a RESTful API, with HTTP requests for all actions, and
uses standard methods of authentication and response selection. 

Use *HTTP Basic* authentication, and specify whether you want XML or JSON
responses by setting the `Accept` header to either `application/json` or `application/xml`.

Choose Your Model
-----------------

The first step is to choose which *model* you want to use for calculation.

Information on all the models in CarbonKit is available at [www.carbonkit.net](https://www.carbonkit.net), with rich searching and tagging functionality to make it easy to find what you want.

For instance, to work out emissions for a flight, you would start by
[searching for flights](https://www.carbonkit.net/search?q=flights). By
looking through the results, you can see that the [Great Circle flight
methodology](https://www.carbonkit.net/categories/Great_Circle_flight_methodology) model deals with flights between two locations. Let's try that one.

To perform a calculation you need two things. 

First is the *model name*. This
can be found in the sidebar for the model, and it's also part of the URL in CarbonKit. 
In this case, it's `Great_Circle_flight_methodology`.

The second are the *context options*. Each model can contain a number of
different contexts, each of which is a variant on a single calculation;
different input values if you like. Each of these is identified by a set
of context options. Again, these are shown on the [code
tab](https://www.carbonkit.net/categories/Great_Circle_flight_methodology/data/code)
for the model. For this model, there is only one context, so the context
options are just `type=great+circle+route`.

Do the Calculation
------------------

Now we can actually perform the calculation.

You simply do a GET request to the URL that represents the model you've
chosen, which includes the model name, and you include the context
options as parameters in order to identify the context. You also include
some input parameters; these are different for every category, but again
the [code
tab](https://www.carbonkit.net/categories/Great_Circle_flight_methodology/data/code)
will tell you what they are.

In this case there are two required inputs, `values.IATACode1`
and `values.IATACode2`, which let us put in airport codes for the
two endpoints of our flight.

#### Request

```shell
curl -H "Accept: application/json" -u username:password \ 
https://api.carbonkit.net/3.6/categories/Great_Circle_flight_methodology/calculation?type=great+circle+route&values.IATAcode1=LHR&values.IATAcode2=LAX
```

#### Response

```json
{
  "status": "OK",
  "output": {
    "amounts": [
      {
        "unit": "kg",
        "default": true,
        "value": 1064.491020315156,
        "type": "totalDirectCO2e"
      },
      {
        "unit": "km",
        "default": false,
        "value": 9550.430830030109,
        "type": "distance"
      },
      {
        "unit": "kg",
        "default": false,
        "value": 1260.561365255674,
        "type": "lifeCycleCO2e"
      },
      {
        "unit": "kg",
        "default": false,
        "value": 1054.0810507104231,
        "type": "CO2"
      },
      {
        "unit": "kg",
        "default": false,
        "value": 10.409969604732819,
        "type": "nitrousOxideCO2e"
      },
      {
        "unit": "kg",
        "default": false,
        "value": 0.0955043083003011,
        "type": "methaneCO2e"
      },
      {
        "unit": "kg",
        "default": false,
        "value": 196.07034494051814,
        "type": "indirectCO2e"
      }
    ],
    "notes": [
      {
        "value": "This methodology provides emissions directly in terms of CO2e. No global warming potentials are applied in this calculation",
        "type": "comment"
      }
    ]
  },
  "version": "3.6.0"
}
```

And that's it! You've just done your first calculation using
the CarbonKit API. A flight from London to LA produces 1065kg of emissions (as shown in the default return value, in the Amount tags).

This is, of course, only a quick overview. The rest of this
documentation goes into much more detail on what it all means and what
more you can do with it, whether you want to just grab the emission
factors, or build up complex histories over time.

<div class="alert alert-info text-center">
Need some help? Email us at <a href="mailto:hello@carbonkit.net">hello@carbonkit.net</a>
</div>
