CarbonKit API Documentation
===========================

How to get started with CarbonKit
---------------------------------

1. Sign up for API credentials at [www.carbonkit.net](https://www.carbonkit.net)
1. Find the model for the activity you want to measure (i.e. transport, electricty, manufacturing processes) on [www.carbonkit.net](https://www.carbonkit.net)
1. Read over the [Quick Start](sections/quickstart.md) and docs to see how to make requests for the model you're using.

How to get help
---------------

Send an email to [hello@carbonkit.net](mailto:hello@carbonkit.net).

Table of Contents
-----------------

### [Introduction](sections/introduction.md)
* [About This Manual](sections/introduction.md#about)
* [Before You Start](sections/introduction.md#before-you-start)
* [API Versions](sections/introduction.md#api-versions)

### [Quick Start](sections/quickstart.md)
* [Getting Started](sections/quickstart.md#getting-started)
* [Choose Your Model](sections/quickstart.md#quick-choose-data-item)
* [Do the Calculation](sections/quickstart.md#quick-do-calculation)

### [Get Emission Factors](sections/data.md)
* [Models and Contexts](sections/data.md#models-and-contexts)
* [Using the CarbonKit website](sections/data.md#using-the-carbonkit-website)
* [Fetching Emission Factors](sections/data.md#fetching-emission-factors)

### [Perform Calculations](sections/calculations.md)
* [Doing a Calculation](sections/calculations.md#doing-a-calculation)
* [Getting Results](sections/calculations.md#getting-results)

### [Store Data](sections/profiles.md)
* [Using the CarbonKit API to Store Data](sections/profiles.md#using-the-carbonkit-api-to-store-data)
* [Creating a Profile](sections/profiles.md#creating-a-profile)
* [Storing Inputs](sections/profiles.md#storing-inputs)
* [Fetching Return Values](sections/profiles.md#fetching-return-values)
* [List Stored Profile Items](sections/profiles.md#list-stored-profile-items)
* [Building Time Series](sections/profiles.md#building-time-series)
* [Naming Profile Items](sections/profiles.md#naming-profile-items)
* [Time Series Queries](sections/profiles.md#time-series-queries)

### [Advanced Options](sections/advanced.md)
* [Specifying API Version](sections/advanced.md#specifying-api-version)
* [Units](sections/advanced.md#units)
* [Matrix Parameters](sections/advanced.md#matrix-parameters)
* [Search](sections/advanced.md#search)
* [Authority](sections/advanced.md#authority)
* [Paging](sections/advanced.md#paging)
* [Interactive Context Selection](sections/advanced.md#interactive-context-selection)

### [CarbonKit API reference](sections/reference.md)
* [Models](sections/reference.md#models)
* [Contexts](sections/reference.md#contexts)
* [Emission Factors](sections/reference.md#emission-factors)
* [Profiles](sections/reference.md#profiles)
* [Profile Items](sections/reference.md#profile-items)
* [Batch Operations](sections/reference.md#batch-operations)
* [Search](sections/reference.md#search)
* [Authentication](sections/reference.md#authentication)
* [UIDs](sections/reference.md#uids)
* [Date and Time Representation](sections/reference.md#date-and-time-representation)

### [References](sections/bib.md)
