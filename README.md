CarbonKit API Documentation
===========================

### Building with [MkDocs](https://www.mkdocs.org/)

* Use pipenv to install the required dependencies

```
pipenv install
```

* Run the dev server to preview the docs

```
pipenv run mkdocs serve
```

* Build the HTML files

```
pipenv run mkdocs build
```

### License

This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/).
